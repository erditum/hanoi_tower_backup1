Start empty gazebo world
roslaunch hanoi_tower_kuka_model start_empty_gazebo_world.launch

Spawn robot sdf model (In this step, make sure that controller are activated)
roslaunch hanoi_tower_kuka_model spawn_robot.launch

In this step, the rviz ready to use.
roslaunch hanoi_tower_kuka_model gazebo_controllers.launch



############

roslaunch hanoi_tower_kuka_model gazebo_world.launch
roslaunch hanoi_tower_kuka_model game_hanoi.launch
roslaunch kuka_iiwa_14_prismatic_gripper_config gazebo_controllers.launch
