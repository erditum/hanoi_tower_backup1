#include <ros/ros.h>

// MoveIt!
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include "std_msgs/String.h"
#include <geometry_msgs/Wrench.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>
#include "tf/transform_datatypes.h"
#include <iostream>
#include <string>
#include <vector> 

boost::shared_ptr<moveit::planning_interface::MoveGroupInterface> move_group;
boost::shared_ptr<moveit::planning_interface::MoveGroupInterface> move_grasp_group;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "robot_moveit_jacobian_matrix");
  ros::AsyncSpinner spinner(1);
  ros::NodeHandle node_handle;
  spinner.start();

    ros::Rate loop_rate(10);

  const std::string PLANNING_GROUP = "iiwa_plan_group";

  const std::string GRASP_PLANNING_GROUP = "grasp_plan_group";

  // The :planning_interface:`MoveGroupInterface` class can be easily
  // setup using just the name of the planning group you would like to control and plan for.
  move_group.reset(new moveit::planning_interface::MoveGroupInterface (PLANNING_GROUP));
  move_grasp_group.reset(new moveit::planning_interface::MoveGroupInterface (GRASP_PLANNING_GROUP));

  // moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
  // moveit::planning_interface::MoveGroupInterface move_grasp_group(GRASP_PLANNING_GROUP)

// We will use the :planning_interface:`PlanningSceneInterface`
  // class to add and remove collision objects in our "virtual world" scene
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

  // Raw pointers are frequently used to refer to the planning group for improved performance.
  const moveit::core::JointModelGroup* joint_model_group =
      move_group->getCurrentState()->getJointModelGroup(PLANNING_GROUP);
    
  robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
  robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
  ROS_INFO("Model frame: %s", kinematic_model->getModelFrame().c_str());
  robot_state::RobotStatePtr kinematic_state(new robot_state::RobotState(kinematic_model));


  Eigen::Vector3d reference_point_position(0.0, 0.0, 0.0);
  Eigen::MatrixXd jacobian;
  int count = 0;

    while (ros::ok())
  {

  kinematic_state->getJacobian(joint_model_group,
                              kinematic_state->getLinkModel(joint_model_group->getLinkModelNames().back()),
                              reference_point_position, jacobian);
    ROS_INFO_STREAM("Jacobian: \n" << jacobian << "\n");


//     std::stringstream ss;
//     ss << "hello world " << count;
//     msg.data = ss.str();
//     chatter_pub.publish(msg);





    ros::spinOnce();
    loop_rate.sleep();
  }
 

  // END_TUTORIAL
  return 0;
}