#! /usr/bin/env python
import sys
import rospy
import geometry_msgs.msg
# sys.path.append("/home/erdi/Desktop/Storage/Temporary/GIT/Github/moveit/moveit_commander/src")
import moveit_commander
import moveit_msgs.msg
import copy
# rospy.sleep(10)



class HanoiTowerPickup(object):
    def __init__(self,namespace,robot_description):
        super(HanoiTowerPickup,self).__init__()
        self.namespace = namespace
        self.robot_description = robot_description

        # Initialize the moveit commander and rospy
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('move_group_python_interface_for_hanoi_tower', anonymous=True)

        #Initialize robot commander which provides information about robot's kinematic model and robot's current joint states
        self.robot = moveit_commander.RobotCommander(ns=self.namespace, robot_description=self.robot_description)

        ## Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        ## for getting, setting, and updating the robot's internal understanding of the
        ## surrounding world:
        self.scene = moveit_commander.PlanningSceneInterface(ns=self.namespace)

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to a planning group (group of joints).  In this tutorial the group is the primary
        ## arm joints in the Panda robot, so we set the group's name to "panda_arm".
        ## If you are using a different robot, change this value to the name of your robot
        ## arm planning group.
        ## This interface can be used to plan and execute motions:

        iiwa_group_name = "iiwa_plan_group"
        self.move_group_joints = moveit_commander.MoveGroupCommander(iiwa_group_name,ns=self.namespace,robot_description=self.robot_description,wait_for_servers=10)
        grasp_group_name = "grasp_plan_group"
        self.move_group_gripper = moveit_commander.MoveGroupCommander(grasp_group_name,ns=self.namespace,robot_description=self.robot_description,wait_for_servers=10)
        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        self.display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=20)


    def get_basic_information(self):
        # robot:
        print ("============ Printing robot state")
        print (self.robot.get_current_state())
        print("End effector link")
        print (self.move_group_joints.get_end_effector_link())
        print("Current Positon")
        current_pose = self.move_group_joints.get_current_pose().pose
        print(current_pose)
        print("robot pose")
        print(self.robot.get_current_state())

    def plan_cartesian_path(self, scale=1):
        waypoints = []

        wpose = self.move_group_joints.get_current_pose().pose
        wpose.position.z -= scale * 0.1  # First move up (z)
        wpose.position.y += scale * 0.2  # and sideways (y)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x += scale * 0.1  # Second move forward/backwards in (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.y -= scale * 0.1  # Third move sideways (y)
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = self.move_group_joints.compute_cartesian_path(
                                                            waypoints,   # waypoints to follow
                                                            0.01,        # eef_step
                                                            0.0)         # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    def display_trajectory(self,plan):
        print("display_trajectory() is executed")
        robot = self.robot
        display_trajectory_publisher = self.display_trajectory_publisher
        display_trajectory = moveit_msgs.msg.DisplayTrajectory()
        display_trajectory.trajectory_start = robot.get_current_state()
        display_trajectory.trajectory.append(plan)
        # Publish
        display_trajectory_publisher.publish(display_trajectory);

    def go_to_pose_goal(self):

        move_group = self.move_group_joints
        pose_target = geometry_msgs.msg.Pose()
        pose_target.orientation.x = -0.00688338920234
        pose_target.orientation.y = 0.999950965998
        pose_target.orientation.z = 0.00107987017122
        pose_target.orientation.w = 0.00703693354945

        pose_target.position.x = 0.040
        pose_target.position.y = 0.60
        pose_target.position.z =  0.65

        move_group.set_pose_target(pose_target)
        plan = move_group.go(wait=True)

        move_group.stop()
        # It is always good to clear your targets after planning with poses.
        # Note: there is no equivalent function for clear_joint_value_targets()
        move_group.clear_pose_targets()
        current_pose = move_group.get_current_pose().pose

    def move_weiter_test(self):
        print('Opening Gripper')
        self.move_group_gripper.set_named_target("gripper_open")
        self.move_group_gripper.go()
        
        move_group = self.move_group_joints
        pose_target = geometry_msgs.msg.Pose()
        pose_target.orientation.x = -0.00688338920234
        pose_target.orientation.y = 0.999950965998
        pose_target.orientation.z = 0.00107987017122
        pose_target.orientation.w = 0.00703693354945

        pose_target.position.x = 0.040
        pose_target.position.y = 0.60
        pose_target.position.z =  0.75
        # move_group.set_pose_target(pose_target)
        # plan = move_group.go(wait=True)
        # move_group.stop()

        while pose_target.position.z >= 0.57:
            pose_target.position.z = pose_target.position.z - 0.03
            move_group.set_pose_target(pose_target)
            plan = move_group.go(wait=True)
            print("Z:",pose_target.position.z)

        self.move_group_gripper.set_named_target("gripper_closed")
        self.move_group_gripper.go()
        print("gripper closed")
        move_group.set_pose_target(pose_target)
        plan = move_group.go(wait=True)

        move_group.stop()

        pose_target = geometry_msgs.msg.Pose()
        pose_target.orientation.x = -0.00688338920234
        pose_target.orientation.y = 0.999950965998
        pose_target.orientation.z = 0.00107987017122
        pose_target.orientation.w = 0.00703693354945

        pose_target.position.x = 0.040
        pose_target.position.y = 0.60
        pose_target.position.z =  0.75
        move_group.set_pose_target(pose_target)
        plan = move_group.go(wait=True)
        move_group.stop()

        # It is always good to clear your targets after planning with poses.
        # Note: there is no equivalent function for clear_joint_value_targets()
        move_group.clear_pose_targets()
        current_pose = move_group.get_current_pose().pose

        
                                                            
def main():
    moveit_kuka = HanoiTowerPickup(namespace="/iiwa", robot_description="/iiwa/robot_description")
    moveit_kuka.get_basic_information()
    # moveit_kuka.go_to_pose_goal()
    moveit_kuka.move_weiter_test()
    # plan, fraction = moveit_kuka.plan_cartesian_path(scale=1)
    # moveit_kuka.display_trajectory(plan)
    rospy.sleep(1)
    moveit_commander.roscpp_shutdown()


if __name__ == '__main__':
    main()


# iiwa_group_name = "iiwa_plan_group"
# iiwa_group = moveit_commander.MoveGroupCommander(iiwa_group_name, ns="/iiwa", robot_description="/iiwa/robot_description")
# grasp_group_name = "grasp_plan_group"
# grasp_group = moveit_commander.MoveGroupCommander(grasp_group_name, ns="/iiwa",robot_description="/iiwa/robot_description")

# grasp_group.set_named_target("gripper_open")
# plan2 = grasp_group.go()

# print("Gripper is open")

# # rospy.sleep(1)

# pose_target = geometry_msgs.msg.Pose()
# pose_target.orientation.x = -0.00688338920234
# pose_target.orientation.y = 0.999950965998
# pose_target.orientation.z = 0.00107987017122
# pose_target.orientation.w = 0.00703693354945

# pose_target.position.x = 0.045
# pose_target.position.y = 0.60
# pose_target.position.z =  0.65
# iiwa_group.set_pose_target(pose_target)
# plan1 = iiwa_group.go()

# print("First Task is done")
# print("Gripper is going to close")

# # rospy.sleep(2)

# # Open the gripper
# grasp_group.set_named_target("gripper_closed")
# plan2 = grasp_group.go()

# print("Gripper is closed")

# # rospy.sleep(2)
# pose_target = geometry_msgs.msg.Pose()
# pose_target.orientation.x = -0.00688338920234
# pose_target.orientation.y = 0.999950965998
# pose_target.orientation.z = 0.00107987017122
# pose_target.orientation.w = 0.00703693354945

# pose_target.position.x = 0.045
# pose_target.position.y = 0.60
# pose_target.position.z =  0.755
# iiwa_group.set_pose_target(pose_target)
# plan1 = iiwa_group.go()

# # # put the arm at the 2nd grasping position
# # pose_target.position.z = 1.125
# # arm_group.set_pose_target(pose_target)
# # plan1 = arm_group.go()
# print("TEST Printer")
# print(iiwa_group.get_current_pose())
# print("TEST Printer")


