"""
ROS part of the External Ros Modules API version 2.0.0
"""

__author__ = 'Omer Yilmaz'

from .external_module import ExternalModule
